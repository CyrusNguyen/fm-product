﻿using FB.User.Business.Interfaces;
using FB.User.Repository;
using FB.User.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FB.User.Business
{
    public class CostBUS : ICostBUS
    {
        public readonly ICostRepositoty _costRepositoty;

        public CostBUS(ICostRepositoty costRepositoty) 
        {
            _costRepositoty = costRepositoty ?? throw new ArgumentNullException(nameof(costRepositoty));
        }

        public void TestCallMethod()
        {
            _costRepositoty.MethodChung();
            _costRepositoty.MethodRieng();
        }
    }
}
