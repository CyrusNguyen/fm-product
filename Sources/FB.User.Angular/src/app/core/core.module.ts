import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BaseService } from './services/base.service';
import { CostService } from './services/cost.service';
import { ItemService } from './services/item.service';
import { MemberService } from './services/member.service';
import { PromotionService } from './services/promotion.service';
import { UserService } from './services/user.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [

        //#region Service Other
        BaseService,
        //#endregion

        //#region API Service
        CostService,
        ItemService,
        MemberService,
        PromotionService,
        UserService
        //#endregion
    ],
    declarations: []
})
export class CoreModule { }
