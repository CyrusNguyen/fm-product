import { throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

export class BaseService {

    apiArress = 'http://localhost:5050';

    httpOptions = {
        headers: new HttpHeaders({
            // tslint:disable-next-line: object-literal-key-quotes
            'Accept': '*/*',
            'Content-Type': 'application/json',
        })
    };

    constructor() { }

    public handleError(error: any) {
        console.error(error);

        return throwError(error);
    }
}
