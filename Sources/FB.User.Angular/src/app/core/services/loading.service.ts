import {
    ApplicationRef,
    ComponentFactoryResolver,
    ComponentRef,
    EmbeddedViewRef,
    Injectable,
    Injector,
} from '@angular/core';
import { LoadingComponent } from 'src/app/shared/components';


@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    private isLoading = false;
    private loadingComponentRef: ComponentRef<LoadingComponent> = undefined;

    constructor(
        private factoryResolver: ComponentFactoryResolver,
        private applicationRef: ApplicationRef,
        private injector: Injector
    ) { }

    show() {
        if (!this.isLoading) {
            this.isLoading = true;

            const factory = this.factoryResolver.resolveComponentFactory(LoadingComponent);

            this.loadingComponentRef = factory.create(this.injector);

            this.loadingComponentRef.onDestroy(() => {
                this.isLoading = false;
            });

            this.applicationRef.attachView(this.loadingComponentRef.hostView);

            const domElement = (this.loadingComponentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

            document.body.appendChild(domElement);
        }
    }

    hide() {
        if (this.isLoading) {
            this.applicationRef.detachView(this.loadingComponentRef.hostView);

            this.loadingComponentRef.destroy();
        }
    }
}
