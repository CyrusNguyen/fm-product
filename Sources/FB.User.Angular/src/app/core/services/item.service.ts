import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable()
export class ItemService {

    itemUrl = '';

    constructor(
        private http: HttpClient,
        private baseService: BaseService
    ) {
        this.itemUrl = `${baseService.apiArress}/api/item`;
     }
}
