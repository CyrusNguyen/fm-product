import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isShowLogin: boolean;

  constructor() {

  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
    this.isShowLogin = localStorage.getItem('token') ? false : true;
  }
}
