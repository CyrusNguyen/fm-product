import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/*
 Material Angular
 */
import { MatButtonModule, MatCheckboxModule, MatNativeDateModule, MatTreeModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CoreModule } from './core/core.module';
import { LoadingComponent } from './shared/components';
import { ItemModule } from './modules/item/item.module';
import { NavComponent } from './shared/components/nav/nav.component';
import { ReportModule } from './modules/report/report.module';
import { MemberModule } from './modules/member/member.module';
import { PitchUserModule } from './modules/pitch-user/pitch-user.module';
import { PitchBookModule } from './modules/pitch-book/pitch-book.module';
import { PitchPriceModule } from './modules/pitch-price/pitch-price.module';
import { CostModule } from './modules/cost/cost.module';
import { ConstantsModule } from './shared/commons/constants.common';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    NavComponent,
  ],
  imports: [

    //#region Base Module
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    //#endregion


    //#region Material Angular
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatTreeModule,
    CdkTreeModule,
    MatNativeDateModule,
    //#endregion

    //#region AppModules
    CoreModule,
    ItemModule,
    ReportModule,
    MemberModule,
    CostModule,
    PitchUserModule,
    PitchBookModule,
    PitchPriceModule,
    //#endregion

  ],
  providers: [
    ConstantsModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoadingComponent,
    NavComponent
  ]
})
export class AppModule { }
