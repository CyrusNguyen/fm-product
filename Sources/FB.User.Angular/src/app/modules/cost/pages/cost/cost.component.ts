import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatPaginator } from '@angular/material';
import { CostCreateComponent } from '../components/cost-create/cost-create.component';

@Component({
  selector: 'app-cost',
  templateUrl: './cost.component.html',
  styleUrls: ['./cost.component.scss']
})
export class CostComponent implements OnInit {

  formSearch: FormGroup;

  displayedColumns: string[] = ['ID', 'Name', 'Unit', 'Quantity', 'UnitPrice', 'Total', 'Note', 'StatusID', 'Action'];

  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, null) sort: MatSort;
  @ViewChild(MatPaginator, null) paging: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.paging._intl.itemsPerPageLabel = 'Dữ liệu';

    this.initializeFormSearch();

    this.setDateDefault();
  }

  initializeFormSearch(): void {
    this.formSearch = this.formBuilder.group({
      FromDate: [null, null],
      ToDate: [null, null]
    });
  }

  setDateDefault(): void {
    const currentDate = new Date();

    this.formSearch.patchValue({
      FromDate: currentDate,
      ToDate: currentDate
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  create(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CostCreateComponent, dialogConfig);
  }
}
