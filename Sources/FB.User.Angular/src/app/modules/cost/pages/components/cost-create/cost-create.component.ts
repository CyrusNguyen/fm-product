import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CostService } from 'src/app/core/services/cost.service';
import { ConstantsModule } from 'src/app/shared/commons/constants.common';
import '../../../../../shared/commons/extension.common';

@Component({
  selector: 'app-cost-create',
  templateUrl: './cost-create.component.html',
  styleUrls: ['./cost-create.component.scss']
})
export class CostCreateComponent implements OnInit {

  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public email: string,
    private dialogRef: MatDialogRef<CostCreateComponent>,
    private formBuilder: FormBuilder,
    private costService: CostService,
    // private notificationService: NotificationService
  ) { }

  get Description() { return this.form.get('Description'); }
  get Quantity() { return this.form.get('Quantity'); }
  get Price() { return this.form.get('Price'); }
  get Total() { return this.form.get('Total'); }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.form = this.formBuilder.group({
      Description: [null, Validators.compose([
        Validators.required,
        Validators.min(0),
        Validators.maxLength(300)
      ])],
      Quantity: [null, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.maxLength(11),
        Validators.pattern(ConstantsModule.GetRegexNumber)
      ])],
      Price: [null, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.maxLength(18),
        Validators.pattern(ConstantsModule.GetRegexNumber)
      ])],
      Total: [null, Validators.compose([
        Validators.required,
        Validators.min(1),
        Validators.maxLength(18),
        Validators.pattern(ConstantsModule.GetRegexNumber)
      ])]
    });
  }

  setForm() {
    this.form.patchValue({
      Description: '',
      Quantity: 0,
      Price: 0,
      Total: 0,
    });
  }

  calcuTotal() {

    const quantity = this.form.value.Quantity.toString().formatCurrencyToNumber();

    const price = this.form.value.Price.toString().formatCurrencyToNumber();

    const total = price * quantity;

    this.form.patchValue({
      Total: total.toString().formatCurrency(),
      Price: price.toString().formatCurrency(),
      Quantity: quantity.toString().formatCurrency()
    });
  }

  formatTotalCurrency(value) {
    this.form.patchValue({
      Total: value.toString().formatCurrency(),
    });
  }

  save(values) {

    // const cost = new CostCreate();
    // cost.Name = values.Name;
    // cost.Note = values.Note;
    // cost.ModifiedTime = new Date();
    // cost.Unit = values.Unit;
    // cost.Quantity = values.Quantity.toString().formatCurrencyToNumber();
    // cost.Total = values.Total.toString().formatCurrencyToNumber();
    // cost.UnitPrice = values.UnitPrice.toString().formatCurrencyToNumber();
    // cost.ModifiedUserID = this._email;
    // cost.StatusID = Statuses.New;

    // this._costService.create(cost).subscribe((data) => {
    //   if (data != null) {
    //     this._notificationService.showSuccess('Thêm thành công');
    //     this._dialogRef.close('success');
    //   }
    // }, (error) => {
    //   this._notificationService.showError(error);
    // });
  }

  close(): void {
    this.dialogRef.close();
  }
}
