import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CostRoutes } from './cost.routing';
import { CostComponent } from './pages/cost/cost.component';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule
} from '@angular/material';
import { CostCreateComponent } from './pages/components/cost-create/cost-create.component';
import { CostEditComponent } from './pages/components/cost-edit/cost-edit.component';

@NgModule({
  imports: [
    CommonModule,
    CostRoutes,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule
  ],
  exports: [
    RouterModule,
    CostComponent
  ],
  declarations: [
    CostComponent,
    CostCreateComponent,
    CostEditComponent
  ],
  entryComponents: [
    CostCreateComponent,
    CostEditComponent
  ]
})
export class CostModule { }
