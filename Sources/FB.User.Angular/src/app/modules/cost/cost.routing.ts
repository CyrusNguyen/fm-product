import { RouterModule, Routes } from '@angular/router';

import { CostComponent } from './pages/cost/cost.component';

const routes: Routes = [
  {
    path: '',
    component: CostComponent
  },
];

export const CostRoutes = RouterModule.forChild(routes);
