import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule,
  MatCheckboxModule
} from '@angular/material';
import { PromotionRoutes } from './promotion.routing';
import { PromotionComponent } from './pages/promotion/promotion.component';


@NgModule({
  imports: [
    CommonModule,
    PromotionRoutes,
    FormsModule,
    ReactiveFormsModule,

    /* ==================================================== */
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule,
    MatCheckboxModule
  ],
  exports: [
    RouterModule,
    PromotionComponent
  ],
  declarations: [
    PromotionComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ]
})
export class PromotionModule { }
