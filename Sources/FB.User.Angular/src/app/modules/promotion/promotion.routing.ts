import { RouterModule, Routes } from '@angular/router';

import { PromotionComponent } from './pages/promotion/promotion.component';

const routes: Routes = [
  {
    path: '',
    component: PromotionComponent
  },
];

export const PromotionRoutes = RouterModule.forChild(routes);
