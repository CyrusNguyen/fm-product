import { RouterModule, Routes } from '@angular/router';

import { ReportComponent } from './pages/report/report.component';

const routes: Routes = [
  {
    path: '',
    component: ReportComponent
  },
];

export const ReportRoutes = RouterModule.forChild(routes);
