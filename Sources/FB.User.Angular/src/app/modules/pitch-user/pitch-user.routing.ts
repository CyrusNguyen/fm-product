import { RouterModule, Routes } from '@angular/router';

import { PitchUserComponent } from './pages/pitch-user/pitch-user.component';

const routes: Routes = [
  {
    path: '',
    component: PitchUserComponent
  },
];

export const PitchUserRoutes = RouterModule.forChild(routes);
