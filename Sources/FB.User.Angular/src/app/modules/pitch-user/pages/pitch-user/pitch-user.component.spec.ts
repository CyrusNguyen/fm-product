/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PitchUserComponent } from './pitch-user.component';

describe('PitchUserComponent', () => {
  let component: PitchUserComponent;
  let fixture: ComponentFixture<PitchUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PitchUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
