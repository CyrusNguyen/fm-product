import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule,
  MatCheckboxModule
} from '@angular/material';
import { PitchUserRoutes } from './pitch-user.routing';
import { PitchUserComponent } from './pages/pitch-user/pitch-user.component';


@NgModule({
  imports: [
    CommonModule,
    PitchUserRoutes,
    FormsModule,
    ReactiveFormsModule,

    /* ================================================ */
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule,
    MatCheckboxModule
  ],
  exports: [
    RouterModule,
    PitchUserComponent
  ],
  declarations: [
    PitchUserComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ]
})
export class PitchUserModule { }
