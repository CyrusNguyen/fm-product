import { RouterModule, Routes } from '@angular/router';

import { MemberComponent } from './pages/member/member.component';

const routes: Routes = [
  {
    path: '',
    component: MemberComponent
  },
];

export const MemberRoutes = RouterModule.forChild(routes);
