import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule,
  MatCheckboxModule
} from '@angular/material';
import { PitchPriceRoutes } from './pitch-price.routing';
import { PitchPriceComponent } from './pages/pitch-price/pitch-price.component';


@NgModule({
  imports: [
    CommonModule,
    PitchPriceRoutes,
    FormsModule,
    ReactiveFormsModule,

    /* =================================================== */
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule,
    MatCheckboxModule
  ],
  exports: [
    RouterModule,
    PitchPriceComponent
  ],
  declarations: [
    PitchPriceComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ]
})
export class PitchPriceModule { }
