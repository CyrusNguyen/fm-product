import { RouterModule, Routes } from '@angular/router';

import { PitchPriceComponent } from './pages/pitch-price/pitch-price.component';

const routes: Routes = [
  {
    path: '',
    component: PitchPriceComponent
  },
];

export const PitchPriceRoutes = RouterModule.forChild(routes);
