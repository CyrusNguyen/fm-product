import { RouterModule, Routes } from '@angular/router';

import { ItemComponent } from './pages/item/item.component';

const routes: Routes = [
  {
    path: '',
    component: ItemComponent
  },
];

export const ItemRoutes = RouterModule.forChild(routes);
