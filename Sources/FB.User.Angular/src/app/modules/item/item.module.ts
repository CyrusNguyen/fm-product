import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule
} from '@angular/material';
import { ItemCreateComponent } from './pages/components/item-create/item-create.component';
import { ItemEditComponent } from './pages/components/item-edit/item-edit.component';
import { ItemRoutes } from './item.routing';
import { ItemComponent } from './pages/item/item.component';

@NgModule({
  imports: [
    CommonModule,
    ItemRoutes,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule
  ],
  exports: [
    RouterModule,
    ItemComponent
  ],
  declarations: [
    ItemComponent,
    ItemCreateComponent,
    ItemEditComponent
  ],
  entryComponents: [
    ItemCreateComponent,
    ItemEditComponent
  ]
})
export class ItemModule { }
