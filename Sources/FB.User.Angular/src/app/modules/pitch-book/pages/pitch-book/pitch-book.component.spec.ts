/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PitchBookComponent } from './pitch-book.component';

describe('PitchBookComponent', () => {
  let component: PitchBookComponent;
  let fixture: ComponentFixture<PitchBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PitchBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
