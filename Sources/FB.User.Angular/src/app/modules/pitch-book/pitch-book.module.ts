import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule,
  MatCheckboxModule
} from '@angular/material';
import { PitchBookRoutes } from './pitch-book.routing';
import { PitchBookComponent } from './pages/pitch-book/pitch-book.component';


@NgModule({
  imports: [
    CommonModule,
    PitchBookRoutes,
    FormsModule,
    ReactiveFormsModule,

    /* ================================================ */
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatChipsModule,
    MatCheckboxModule
  ],
  exports: [
    RouterModule,
    PitchBookComponent
  ],
  declarations: [
    PitchBookComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ]
})
export class PitchBookModule { }
