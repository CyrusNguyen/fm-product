import { RouterModule, Routes } from '@angular/router';

import { PitchBookComponent } from './pages/pitch-book/pitch-book.component';

const routes: Routes = [
  {
    path: '',
    component: PitchBookComponent
  },
];

export const PitchBookRoutes = RouterModule.forChild(routes);
