import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/core/services/user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    // helper = new j();
    constructor(
        private http: Router,
        private userService: UserService
    ) { }

    canActivate() {
        return true;
        //   next: ActivatedRouteSnapshot,
        //            state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // if (localStorage.getItem('token')) {
        //   if (!this._helper.isTokenExpired(localStorage.getItem('token'))) {
        //     const allowRoles = next.data['roles'] as Array<string>;
        //     if (!allowRoles) {
        //       return true;
        //     }
        //     const result = this.userService.roleMatch(allowRoles);
        //     if (!result) {
        //       this.http.navigateByUrl('/unauthorized');
        //     }
        //     return result;
        //   }
        //   localStorage.removeItem('token');
        // }
        // this.http.navigateByUrl('/login');
        // return false;
    }
}
