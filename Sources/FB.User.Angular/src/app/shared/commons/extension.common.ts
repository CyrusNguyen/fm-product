declare global {
    interface Date {
        convertDateToString(): String;
    }

    interface String {
        formatCurrency(): String;
        formatCurrencyToNumber(): Number;
    }
}

export {}

function convertDateToString(this: Date): String {
    return this.toJSON().substring(0, 10);
}

// Example : 12,123,123 ( String ) => 12123123 ( Number )
function formatCurrencyToNumber(this: string): Number {
    if (this.includes(',')) {
        return Number(this.replace(/,/g, ''));
    }
    return Number(this);
}

// Example : 12123123 ( Number ) => 12,123,123 ( String )
function formatCurrency(this: string): string {
    return this.replace(/[, ]+/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

Date.prototype.convertDateToString = convertDateToString;
String.prototype.formatCurrencyToNumber = formatCurrencyToNumber;
String.prototype.formatCurrency = formatCurrency;



