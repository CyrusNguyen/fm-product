// tslint:disable-next-line: no-empty-interface
export interface BaseModel {
    CreateDate: Date;
    CreateUser: string;
    LastUpdateDate: Date;
    LastUpdateUser: string;
}
