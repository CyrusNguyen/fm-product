import { BaseModel } from './base.model';

export class Cost implements BaseModel {
    CostCode: string;
    Description: string;
    Price: number;
    Quantity: number;
    PitchCode: string;
    CreateDate: Date;
    CreateUser: string;
    LastUpdateDate: Date;
    LastUpdateUser: string;
}
