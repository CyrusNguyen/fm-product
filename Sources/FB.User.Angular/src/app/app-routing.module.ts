import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostModule } from './modules/cost/cost.module';
import { UserModule } from './modules/user/user.module';
import { AuthGuard } from './modules/user/auth/auth.guard';
import { ItemModule } from './modules/item/item.module';
import { PitchPriceModule } from './modules/pitch-price/pitch-price.module';
import { PitchBookModule } from './modules/pitch-book/pitch-book.module';
import { PitchUserModule } from './modules/pitch-user/pitch-user.module';
import { MemberModule } from './modules/member/member.module';
import { PromotionModule } from './modules/promotion/promotion.module';
import { ReportModule } from './modules/report/report.module';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => UserModule
  },
  {
    path: 'cost',
    loadChildren: () => CostModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'item',
    loadChildren: () => ItemModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'pitch-price',
    loadChildren: () => PitchPriceModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'pitch-book',
    loadChildren: () => PitchBookModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'pitch-user',
    loadChildren: () => PitchUserModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'member',
    loadChildren: () => MemberModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'promotion',
    loadChildren: () => PromotionModule
    , canActivate: [AuthGuard]
  },
  {
    path: 'report',
    loadChildren: () => ReportModule
    , canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
