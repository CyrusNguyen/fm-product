CREATE DATABASE FM_User
GO

USE FM_User
GO

CREATE TABLE [Pitch]
(
	[Index]				INT	IDENTITY(1,1),
	[PitchCode]			VARCHAR(20)		PRIMARY KEY,
	[Address]			NVARCHAR(200)	NOT NULL,
	[Description]		NVARCHAR(100),
	[Image]				VARCHAR(200),
	[PhoneNumber]		VARCHAR(10)		NOT NULL UNIQUE,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Pitch
ON [Pitch]([Index]);
GO

ALTER TABLE [Pitch]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Pitch]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [User]
(
	[Index]				INT	IDENTITY(1,1),
	[UserCode]			VARCHAR(20)		PRIMARY KEY,
	[Account]			VARCHAR(50)		NOT NULL,
	[Password]			VARCHAR(MAX)	NOT NULL,
	[SaltPassword]		VARCHAR(10)		NOT NULL,
	[Gmail]				VARCHAR(100)	NOT NULL,
	[Role]				VARCHAR(10)		NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_User
ON [User]([Index]);
GO

ALTER TABLE [User]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [User]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [UserLogin]
(
	[Index]				INT	IDENTITY(1,1),
	[UserCode]			VARCHAR(20)		NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[Status]			VARCHAR(10)		NOT NULL,
	[Device]			NVARCHAR(200)	NOT NULL,
	[LoginTime]			DATETIME		NOT NULL,
	
)
GO

CREATE INDEX idx_UserLogin
ON [UserLogin]([Index]);
GO

ALTER TABLE [UserLogin]
ADD DEFAULT (GETDATE()) FOR [LoginTime]
GO

CREATE TABLE [Member]
(
	[Index]				INT	IDENTITY(1,1) ,
	[MemberCode]		VARCHAR(20)		PRIMARY KEY,
	[MemberName]		NVARCHAR(200)	NOT NULL,
	[PhoneNumber]		VARCHAR(10)		NOT NULL UNIQUE,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Member
ON [Member]([Index]);
GO

ALTER TABLE [Member]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Member]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [Unit]
(
	[Index]				INT	IDENTITY(1,1) ,
	[UnitCode]			VARCHAR(20)		PRIMARY KEY,
	[UnitName]			NVARCHAR(50)	NOT NULL UNIQUE,	
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Unit
ON [Unit]([Index]);
GO

ALTER TABLE [Unit]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Unit]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO


CREATE TABLE [Item]
(
	[Index]				INT	IDENTITY(1,1) ,
	[ItemCode]			VARCHAR(20)		PRIMARY KEY,
	[ItemName]			NVARCHAR(100)	NOT NULL UNIQUE,	
	[UnitCode]			VARCHAR(20)		NOT NULL,
	[Price]				INT				NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Item
ON [Item]([Index]);
GO

ALTER TABLE [Item]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Item]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [Promotion]
(
	[Index]				INT	IDENTITY(1,1),
	[PromotionCode]		VARCHAR(20)		PRIMARY KEY,
	[PromotionName]		NVARCHAR(100)	NOT NULL UNIQUE,
	[Description]		NVARCHAR(300),
	[Discount]			INT				NOT NULL,
	[Type]				VARCHAR(20)		NOT NULL,
	[Active]			BIT				NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Promotion
ON [Promotion]([Index]);
GO

ALTER TABLE [Promotion]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Promotion]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [PitchType]
(
	[Index]				INT	IDENTITY(1,1),
	[PitchTypeCode]		VARCHAR(20)		PRIMARY KEY,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[Description]		NVARCHAR(100),
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_PitchType
ON [PitchType]([Index]);
GO

ALTER TABLE [PitchType]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [PitchType]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO


CREATE TABLE [PitchPrice]
(
	[Index]				INT	IDENTITY(1,1),
	[PitchPriceCode]	VARCHAR(20)		PRIMARY KEY,
	[Weekday]			VARCHAR(20)		NOT NULL,
	[PitchTypeCode]		VARCHAR(20)		NOT NULL,
	[StartTime]			DATETIME		NOT NULL,
	[FinishTime]		DATETIME		NOT NULL,
	[Price]				INT				NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_PitchPrice
ON [PitchPrice]([Index]);
GO

ALTER TABLE [PitchPrice]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [PitchPrice]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [PitchBook]
(
	[Index]				INT	IDENTITY(1,1),
	[PitchBookCode]		VARCHAR(20)		PRIMARY KEY,
	[PitchPriceCode]	VARCHAR(20)		NOT NULL,
	[StartTime]			DATETIME		NOT NULL,
	[FinishTime]		DATETIME		NOT NULL,
	[MemberCode]		VARCHAR(20)		NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[Status]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_PitchBook
ON [PitchBook]([Index]);
GO

ALTER TABLE [PitchBook]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [PitchBook]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [PitchBookDetail]
(
	[Index]					INT	IDENTITY(1,1),
	[PitchBookDetailCode]	VARCHAR(20)		PRIMARY KEY,
	[ItemCode]				VARCHAR(100)	NOT NULL,
	[ItemPrice]				INT				NOT NULL,
	[Quantity]				INT				NOT NULL,
	[CreateUser]			VARCHAR(20)		NOT NULL,
	[CreateUserDate]		DATETIME		NOT NULL,
	[LastUpdateUser]		VARCHAR(20)		NOT NULL,
	[LastUpdateDate]		DATETIME		NOT NULL
)
GO

CREATE INDEX idx_PitchBookDetail
ON [PitchBookDetail]([Index]);
GO

ALTER TABLE [PitchBookDetail]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [PitchBookDetail]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [Cost]
(
	[Index]				INT	IDENTITY(1,1),
	[CostCode]			VARCHAR(20)		PRIMARY KEY,
	[Description]		NVARCHAR(300)	NOT NULL,
	[Price]				INT				NOT NULL,
	[Quantity]			INT				NOT NULL,
	[PitchCode]			VARCHAR(20)		NOT NULL,
	[CreateUser]		VARCHAR(20)		NOT NULL,
	[CreateUserDate]	DATETIME		NOT NULL,
	[LastUpdateUser]	VARCHAR(20)		NOT NULL,
	[LastUpdateDate]	DATETIME		NOT NULL
)
GO

CREATE INDEX idx_Cost
ON [Cost]([Index]);
GO

ALTER TABLE [Cost]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [Cost]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO

CREATE TABLE [PitchBookPromotion]
(
	[Index]						INT	IDENTITY(1,1),
	[PitchBookPromotionCode]	VARCHAR(20)	PRIMARY KEY,
	[PromotionCode]				VARCHAR(20)	NOT NULL,
	[PitchBookCode]				VARCHAR(20)	NOT NULL,
	[CreateUser]				VARCHAR(20)	NOT NULL,
	[CreateUserDate]			DATETIME   	NOT NULL,
	[LastUpdateUser]			VARCHAR(20)	NOT NULL,
	[LastUpdateDate]			DATETIME   	NOT NULL
)
GO

CREATE INDEX idx_PitchBookPromotion
ON [PitchBookPromotion]([Index]);
GO

ALTER TABLE [PitchBookPromotion]
ADD DEFAULT (GETDATE()) FOR [CreateUserDate]
GO

ALTER TABLE [PitchBookPromotion]
ADD DEFAULT (GETDATE()) FOR [LastUpdateDate]
GO