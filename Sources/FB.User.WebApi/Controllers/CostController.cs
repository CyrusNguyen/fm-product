﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FB.User.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FB.User.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CostController : ControllerBase
    {
        public readonly ICostBUS _costBUS;

        public CostController(ICostBUS costBUS)
        {
            _costBUS = costBUS ?? throw new ArgumentNullException(nameof(costBUS));
        }

        public void TestCallMethod()
        {
            _costBUS.TestCallMethod();
        }
    }
}