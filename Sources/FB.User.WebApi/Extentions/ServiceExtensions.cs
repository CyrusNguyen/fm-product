﻿using AutoMapper;
using FB.User.Core.Mapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FB.User.WebApi.Extentions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
        }

        public static void ConfigureRepository(this IServiceCollection services)
        {

        }
        public static void ConfigureBusinessServices(this IServiceCollection services)
        {
        }

        //public static void AutoMapperConfigure(this IServiceCollection services)
        //{
        //    Mapper.Initialize(cfg =>
        //    {
        //        cfg.AddProfile<AutoMapperProfile>();
        //    });
        //}

        public static void AddCustomMvc(this IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
    }
}
