﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FB.User.WebApi.Extentions.ExceptionModels
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException() : base()
        {
        }

        public UnauthorizedException(string message) 
            : base(message)
        {
        }

        public UnauthorizedException(string message, Exception inner) 
            : base(message, inner)
        {
        }
    }
}
