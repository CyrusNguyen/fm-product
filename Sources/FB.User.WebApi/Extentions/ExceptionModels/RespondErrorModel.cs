﻿using Newtonsoft.Json;

namespace FB.User.WebApi.Extentions.ExceptionModels
{
    public class RespondErrorModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }      
    }
}
