﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FB.User.WebApi.Extentions.ExceptionModels;
using System.Net;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FB.User.WebApi.Extentions
{
    public class ExceptionMiddlewareExtensions
    {
        private const string JsonContentType = "application/json";
        private readonly RequestDelegate request;
        private readonly IConfiguration configuration;

        /// <summary>
        /// Initializes a new instance of <see cref="ExceptionHandlerMiddleware" /> class.
        /// </summary>
        /// <param name="next">The next middleware.</param>
        public ExceptionMiddlewareExtensions(RequestDelegate next, IConfiguration configuration)
        {
            this.request = next;
            this.configuration = configuration;
        }

        public Task Invoke(HttpContext context) => this.InvokeAsync(context);

        async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this.request(context);
            }
            catch (Exception ex)
            {
                var httpStatusCode = ConfigureExceptionTypes(ex);

                context.Response.StatusCode = httpStatusCode;
                context.Response.ContentType = JsonContentType;
                context.Response.Headers.Add("Access-Control-Allow-Credentials", new[] { "true" });
                context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { configuration["AllowedHosts"] });

                var error = new RespondErrorModel()
                {
                    StatusCode = httpStatusCode,
                    Message = ex.Message
                };

                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(
                        new { error },
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    })
                );
            }
        }
        private int ConfigureExceptionTypes(Exception ex)
        {
            int httpStatusCode;

            switch (ex)
            {
                case var _ when ex is NotFoundException:
                    httpStatusCode = (int)HttpStatusCode.NotFound;
                    break;
                    
                default:
                    httpStatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            return httpStatusCode;
        }
    }
}
