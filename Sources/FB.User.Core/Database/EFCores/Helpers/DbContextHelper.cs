﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace FB.User.Core.Database.EFCores.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class DbContextHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="storedName"></param>
        /// <returns></returns>
        public static DbCommand SqlStored(this DbContext dbContext, string storedName)
        {
            try
            {
                if (dbContext == null)
                {
                    throw new ArgumentNullException(nameof(dbContext));
                }

                if (string.IsNullOrEmpty(storedName))
                {
                    throw new ArgumentNullException(nameof(storedName));
                }

                var cmd = dbContext.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = storedName;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                return cmd;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DbCommand WithSqlParams(this DbCommand dbCommand, params (string,object)[] parameters)
        {
            try
            {
                if (dbCommand == null)
                {
                    throw new ArgumentNullException(nameof(dbCommand));
                }

                if (parameters == null)
                {
                    throw new ArgumentNullException(nameof(parameters));
                }

                foreach (var param in parameters)
                {
                    var par = dbCommand.CreateParameter();
                    par.ParameterName = param.Item1;
                    par.Value = param.Item2 ?? DBNull.Value;
                    dbCommand.Parameters.Add(par);
                }

                return dbCommand;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbCommand"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static DbCommand WithSqlParams<T>(this DbCommand dbCommand,T parameter) where T : class
        {
            try
            {
                if (dbCommand == null)
                {
                    throw new ArgumentNullException(nameof(dbCommand));
                }

                if (parameter == null)
                {
                    throw new ArgumentNullException(nameof(parameter));
                }

                var properties = parameter.GetType().GetProperties();
                if (properties.Count() == 0)
                {
                    throw new Exception("The parameter does not contain properties.");
                }

                foreach (var param in properties)
                {
                    var par = dbCommand.CreateParameter();
                    par.ParameterName = param.Name;
                    par.Value = param.GetValue(parameter) ?? DBNull.Value;
                    dbCommand.Parameters.Add(par);
                }

                return dbCommand;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbCommand"></param>
        /// <returns></returns>
        public static IList<T> ToExecute<T>(this DbCommand dbCommand) where T : class, new()
        {
            try
            {
                if (dbCommand == null)
                {
                    throw new ArgumentNullException(nameof(dbCommand));
                }

                using (dbCommand)
                {
                    if (dbCommand.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        dbCommand.Connection.Open();
                    }

                    try
                    {
                        using (var reader = dbCommand.ExecuteReader())
                        {
                            return reader.MapToList<T>();
                        }
                    }
                    finally
                    {
                        dbCommand.Connection.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbDataReader"></param>
        /// <returns></returns>
        private static IList<T> MapToList<T>(this DbDataReader dbDataReader) where T : new()
        {
            try
            {
                if (dbDataReader == null)
                {
                    throw new ArgumentNullException(nameof(dbDataReader));
                }

                var props = typeof(T).GetRuntimeProperties();

                var colMapping = dbDataReader.GetColumnSchema()
                    .Where(x => props.Any(y => y.Name.ToLower() == x.ColumnName.ToLower()))
                    .ToDictionary(key => key.ColumnName.ToLower());

                var records = new List<T>();
                if (dbDataReader.HasRows)
                {
                    while (dbDataReader.Read())
                    {
                        T instance = new T();
                        bool IsMapping = false;
                        foreach (var pro in props)
                        {
                            if (colMapping.ContainsKey(pro.Name.ToLower()))
                            {
                                IsMapping = true;
                                var val = dbDataReader.GetValue(colMapping[pro.Name.ToLower()].ColumnOrdinal.Value);
                                pro.SetValue(instance, val == DBNull.Value ? null : val);
                            }
                        }

                        if (IsMapping)
                        {
                            records.Add(instance);
                        }
                    }
                }

                return records.Count == 0 ? null : records;
            }
            catch
            {
                throw;
            }
        }
    }
}
