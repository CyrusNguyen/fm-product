﻿using FB.User.Core.Database.DbRepositories.Interfaces;
using FB.User.Core.Database.EFCores.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace FB.User.Core.Database.DbRepositories
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DbRepository : IDbRepository 
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly DbContext _context;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public DbRepository(DbContext context) 
        {
            _context = context as DbContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public DbSet<TEntity> Entity<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storedName"></param>
        /// <returns></returns>
        public DbCommand SqlStored(string storedName)
        {
            return this._context.SqlStored(storedName);
        }
    }
}
