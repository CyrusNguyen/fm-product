﻿using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace FB.User.Core.Database.DbRepositories.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDbRepository
    {
        DbSet<TEntity> Entity<TEntity>() where TEntity : class;

        DbCommand SqlStored(string storedName);
    }
}
