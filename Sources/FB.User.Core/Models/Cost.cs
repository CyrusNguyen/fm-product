﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class Cost
    {
        /// <summary>
        /// Code of cost.
        /// </summary>
        [Required(ErrorMessage = "Mã chi phí không được trống")]
        [MaxLength(20, ErrorMessage = "Mã chi phí không được có kí tự nhiều hơn 20")]
        public string CostCode { get; set; }

        /// <summary>
        /// Description of cost
        /// </summary>
        [Required(ErrorMessage = "Miêu tả không được trống")]
        [MaxLength(300, ErrorMessage = "Miêu tả không được có kí tự nhiều hơn 300")]
        public string Description { get; set; }

        /// <summary>
        /// Price of cost.
        /// </summary>
        [Required(ErrorMessage = "Giá tiền không được trống")]
        [Range(0, 1000000000, ErrorMessage = "Giá tiền khoảng từ 0 đến 1 tỷ")]
        public int Price { get; set; }

        /// <summary>
        /// Quantity of cost.
        /// </summary>
        [Required(ErrorMessage = "Số lượng không được trống")]
        [Range(0, 10000, ErrorMessage = "Số lượng khoảng từ 0 đến 10,000")]
        public int Quantity { get; set; }
    }
}