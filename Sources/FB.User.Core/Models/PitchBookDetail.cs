﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class PitchBookDetail
    {
        /// <summary>
        /// Code of book pitch detail.
        /// </summary>
        [Required(ErrorMessage = "Mã chi tiết đặt sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã chi tiết đặt sân không được có kí tự nhiều hơn 20")]
        public string PitchBookDetailCode { get; set; }

        /// <summary>
        /// Code of book pitch belong pitch book detail.
        /// </summary>
        [Required(ErrorMessage = "Mã đặt sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã đặt sân không được có kí tự nhiều hơn 20")]
        public string PitchBookCode { get; set; }

        /// <summary>
        /// Code of price pitch belong pitch book detail.
        /// </summary>
        [Required(ErrorMessage = "Mã giá sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã giá sân không được có kí tự nhiều hơn 20")]
        public string PitchPriceCode { get; set; }

        /// <summary>
        /// Code of item belong pitch book detal.
        /// </summary>
        [Required(ErrorMessage = "Mã dịch vụ không được trống")]
        [MaxLength(20, ErrorMessage = "Mã dịch vụ không được có kí tự nhiều hơn 20")]
        public string ItemCode { get; set; }

        /// <summary>
        /// Price of item.
        /// </summary>
        [Required(ErrorMessage = "Giá tiền không được trống")]
        [Range(0, 1000000000, ErrorMessage = "Giá tiền khoảng từ 0 đến 1 tỷ")]
        public int ItemPrice { get; set; }

        /// <summary>
        /// Quantity of item.
        /// </summary>
        [Required(ErrorMessage = "Số lượng không được trống")]
        [Range(0, 10000, ErrorMessage = "Số lượng khoảng từ 0 đến 10,000")]
        public int Quantity { get; set; }
    }
}