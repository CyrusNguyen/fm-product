﻿using System;

namespace FB.User.Core.Models
{
    public interface IBaseEntity
    {
        string PitchCode { get; set; }
        string CreateUser { get; set; }
        DateTime CreateUserDate { get; set; }
        string LastUpdateUser { get; set; }
        DateTime LastUpdateDate { get; set; }
    }
}