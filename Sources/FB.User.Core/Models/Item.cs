﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class Item
    {
        /// <summary>
        /// Code of item.
        /// </summary>
        [Required(ErrorMessage = "Mã dịch vụ không được trống")]
        [MaxLength(20, ErrorMessage = "Mã dịch vụ không được có kí tự nhiều hơn 20")]
        public string ItemCode { get; set; }

        /// <summary>
        /// Name or description of item.
        /// </summary>
        [Required(ErrorMessage = "Tên dịch vụ không được trống")]
        [MaxLength(100, ErrorMessage = "Tên dịch vụ không được có kí tự nhiều hơn 100")]
        public string ItemName { get; set; }

        /// <summary>
        /// Unit of item.
        /// </summary>
        [Required(ErrorMessage = "Mã đơn vị tính không được trống")]
        [MaxLength(20, ErrorMessage = "Mã đơn vị tính không được có kí tự nhiều hơn 20")]
        public string UnitCode { get; set; }

        /// <summary>
        /// Price of item.
        /// </summary>
        [Required(ErrorMessage = "Giá tiền không được trống")]
        [Range(0, 1000000000, ErrorMessage = "Giá tiền khoảng từ 0 đến 1 tỷ")]
        public int Price { get; set; }

        /// <summary>
        /// Status of item.
        /// </summary>
        [Required(ErrorMessage = "Trạng thái không được trống")]
        [MaxLength(20, ErrorMessage = "Trạng thái không được có kí tự nhiều hơn 20")]
        public string Status { get; set; }
    }
}