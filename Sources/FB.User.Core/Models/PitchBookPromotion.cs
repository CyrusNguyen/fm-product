﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class PitchBookPromotion
    {
        /// <summary>
        /// Code of pitch book promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã khuyến mãi đặt sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã khuyến mãi đặt sân không được có kí tự nhiều hơn 20")]
        public string PitchBookPromotionCode { get; set; }

        /// <summary>
        /// Code of pitch book belong pitch book promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã đặt sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã đặt sân không được có kí tự nhiều hơn 20")]
        public string PitchBookCode { get; set; }

        /// <summary>
        /// Code of promotion belong pitch book promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã khuyến mãi không được trống")]
        [MaxLength(20, ErrorMessage = "Mã khuyến mãi không được có kí tự nhiều hơn 20")]
        public string PromotionCode { get; set; }
    }
}