﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class PitchBook
    {
        /// <summary>
        /// Code of book pitch.
        /// </summary>
        [Required(ErrorMessage = "Mã đặt sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã đặt sân không được có kí tự nhiều hơn 20")]
        public string PitchBookCode { get; set; }

        /// <summary>
        /// Start time when book pitch.
        /// </summary>
        [Required(ErrorMessage = "Thời gian bắt đầu không được trống")]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Finish time when book pitch.
        /// </summary>
        [Required(ErrorMessage = "Thời gian kết thúc không được trống")]
        public DateTime FinishTime { get; set; }

        /// <summary>
        /// Code of pitch price belong pitch book.
        /// </summary>
        [Required(ErrorMessage = "Mã giá sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã giá sân không được có kí tự nhiều hơn 20")]
        public string PitchPriceCode { get; set; }

        /// <summary>
        /// Code of member belong pitch book.
        /// </summary>
        [Required(ErrorMessage = "Mã thành viên không được trống")]
        [MaxLength(20, ErrorMessage = "Mã thành viên không được có kí tự nhiều hơn 20")]
        public string MemberCode { get; set; }

        /// <summary>
        /// Status of pitch book.
        /// </summary>
        [Required(ErrorMessage = "Trạng thái không được trống")]
        [MaxLength(20, ErrorMessage = "Trạng thái không được có kí tự nhiều hơn 20")]
        public string Status { get; set; }
    }
}