﻿using System;

namespace FB.User.Core.Models
{
    public class UserLogin
    {
        /// <summary>
        /// Code of user.
        /// </summary>
        public string UserCode { get; set; }

        /// <summary>
        /// Device login of user.
        /// </summary>
        public string Device { get; set; }

        /// <summary>
        /// Time login of user.
        /// </summary>
        public DateTime LoginTime { get; set; }

        /// <summary>
        /// Status login of user.
        /// </summary>
        public string Status { get; set; }
    }
}