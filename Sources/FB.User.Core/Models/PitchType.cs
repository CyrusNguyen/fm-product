﻿namespace FB.User.Core.Models
{
    public class PitchType
    {
        /// <summary>
        /// Code of pitch type.
        /// </summary>
        public string PitchTypeCode { get; set; }

        /// <summary>
        /// Description pitch type.
        /// </summary>
        public string Description { get; set; }
    }
}