﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class PitchPrice
    {
        /// <summary>
        /// Code of pitch price.
        /// </summary>
        [Required(ErrorMessage = "Mã chi phí không được trống")]
        [MaxLength(20, ErrorMessage = "Mã chi phí không được có kí tự nhiều hơn 20")]
        public string PitchPriceCode { get; set; }

        /// <summary>
        /// Weekday of pitch.
        /// </summary>
        [Required(ErrorMessage = "Mã chi phí không được trống")]
        [MaxLength(20, ErrorMessage = "Mã chi phí không được có kí tự nhiều hơn 20")]
        public string Weekday { get; set; }

        /// <summary>
        /// Time start.
        /// </summary>
        [Required(ErrorMessage = "Thời gian bắt dầu không được trống")]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Time finish.
        /// </summary>
        [Required(ErrorMessage = "Thời gian kết thúc không được trống")]
        public DateTime FinishTime { get; set; }

        /// <summary>
        /// Code of pitch type.
        /// </summary>
        [Required(ErrorMessage = "Loại sân không được trống")]
        [MaxLength(20, ErrorMessage = "Mã chi phí không được có kí tự nhiều hơn 20")]
        public string PitchTypeCode { get; set; }

        /// <summary>
        /// Price of pitch.
        /// </summary>
        [Required(ErrorMessage = "Giá sân không được trống")]
        [Range(0, 1000000000, ErrorMessage = "Giá sân khoảng từ 0 đến 1 tỷ")]
        public int Price { get; set; }
    }
}