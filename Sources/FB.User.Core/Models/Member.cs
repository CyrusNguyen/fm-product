﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class Member
    {
        /// <summary>
        /// Code of member.
        /// </summary>
        [Required(ErrorMessage = "Mã khách hàng không được trống")]
        [MaxLength(20, ErrorMessage = "Mã khách hàng không được có kí tự nhiều hơn 20")]
        public string MemberCode { get; set; }

        /// <summary>
        /// Phone number of member.
        /// </summary>
        [Required(ErrorMessage = "Số điện thoại không được trống")]
        [MaxLength(20, ErrorMessage = "Số điện thoại không được có kí tự nhiều hơn 10")]
        [Phone(ErrorMessage = "Số điện chưa đúng định dạng")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Code of item.
        /// </summary>
        [Required]
        public bool IsDeleted { get; set; }
    }
}