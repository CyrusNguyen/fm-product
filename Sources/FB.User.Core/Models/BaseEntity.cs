﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class BaseEntity : IBaseEntity
    {
        /// <summary>
        /// Code of pitch
        /// </summary>
        [Required]
        [MaxLength(20, ErrorMessage = "Mã sân không được có kí tự nhiều hơn 20")]
        public string PitchCode { get; set; }

        /// <summary>
        /// User create data.
        /// </summary>
        [Required]
        [MaxLength(20, ErrorMessage = "Mã người dùng không được có kí tự nhiều hơn 20")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Date user create data.
        /// </summary>
        public DateTime CreateUserDate { get; set; }

        /// <summary>
        /// User update last data.
        /// </summary>
        [Required]
        [MaxLength(20, ErrorMessage = "Mã người dùng không được có kí tự nhiều hơn 20")]
        public string LastUpdateUser { get; set; }

        /// <summary>
        /// Date user update last data.
        /// </summary>
        public DateTime LastUpdateDate { get; set; }
    }
}