﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class Promotion
    {
        /// <summary>
        /// Code of promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã khuyến mãi không được trống")]
        [MaxLength(20, ErrorMessage = "Mã khuyến mãi không được có kí tự nhiều hơn 20")]
        public string PromotionCode { get; set; }

        /// <summary>
        /// Name of promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã khuyến mãi không được trống")]
        [MaxLength(100, ErrorMessage = "Mã khuyến mãi không được có kí tự nhiều hơn 100")]
        public string PromotionName { get; set; }

        /// <summary>
        /// Name of promotion.
        /// </summary>
        [Required(ErrorMessage = "Mã khuyến mãi không được trống")]
        [MaxLength(100, ErrorMessage = "Mã khuyến mãi không được có kí tự nhiều hơn 100")]
        public string Description { get; set; }

        /// <summary>
        /// Discount of promotion.
        /// </summary>
        [Required(ErrorMessage = "Giá khuyến mãi không được trống")]
        [Range(0, 100, ErrorMessage = "Giá khuyến mãi khoảng từ 0 đến 100")]
        public int Discount { get; set; }

        /// <summary>
        /// Type of promotion.
        /// </summary>
        [Required(ErrorMessage = "Loại khuyến mãi không được trống")]
        [MaxLength(20, ErrorMessage = "Loại khuyến mãi không được có kí tự nhiều hơn 20")]
        public string Type { get; set; }

        /// <summary>
        /// Status active of promotion.
        /// </summary>
        [Required(ErrorMessage = "Trạng thái hoạt động không được trống")]
        public bool Active { get; set; }
    }
}