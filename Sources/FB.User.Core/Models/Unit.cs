﻿using System.ComponentModel.DataAnnotations;

namespace FB.User.Core.Models
{
    public class Unit
    {
        /// <summary>
        /// Code of unit.
        /// </summary>
        [Required(ErrorMessage = "Mã đơn vị tính không được trống")]
        [MaxLength(20, ErrorMessage = "Mã đơn vị tính không được có kí tự nhiều hơn 20")]
        public string UnitCode { get; set; }

        /// <summary>
        /// Name of unit.
        /// </summary>
        [Required(ErrorMessage = "Tên đơn vị tính không được trống")]
        [MaxLength(50, ErrorMessage = "Tên đơn vị tính không được có kí tự nhiều hơn 50")]
        public string UnitName { get; set; }
    }
}