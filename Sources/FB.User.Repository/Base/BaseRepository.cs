﻿using FB.User.Core.Database.DbRepositories.Interfaces;
using FB.User.Repository.Interfaces;
using System;

namespace FB.User.Repository.Base
{
    public class BaseRepository : IBaseRepositoty
    {
        protected readonly IDbRepository _dbRepository;

        public BaseRepository(IDbRepository dbRepository)
        {
            _dbRepository = dbRepository ?? throw new ArgumentNullException(nameof(dbRepository));
        }

        public void MethodChung()
        {
            throw new NotImplementedException();
        }
    }
}